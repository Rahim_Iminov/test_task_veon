angular.module('test_task_veon', [])

	.controller('MainCtrl', MainCtrl);

	MainCtrl.$inject = ['$http', '$scope'];

	function MainCtrl($http, $scope) {
		var vm = this;

		vm.progress; 	// шкала заполнения
 		vm.quotes; 		// массив цитат
 		vm.text; 		// текст цитаты
		vm.showError;	// переменная, для отображения ошибки
		vm.ErrorText;   // динамич. текст ошибки
		vm.showWArray = [];
		var index;

		//Функция для высчитывания процента
        function countProgress(){
    		vm.progress = (vm.quotes.length * 100)/10;
        }


        vm.getIndex = function(quote, indexArr) {
        	index = vm.quotes.indexOf(quote);

	        for(var i = 0; i < vm.showWArray.length; i++){
	        	vm.showWArray[i] = false;
	        }
	        vm.showWArray[indexArr] = true;

        }



        $http.get('/api/quotes').success(function(response) {
            vm.quotes = response;
		    countProgress();
            console.log(vm.quotes);
        });

		vm.addQuote = function() {
			if(vm.quotes.length == 10){
				vm.ErrorText = "Для добавления новых цитат удалите одну из добавленных!";
				vm.showError = true;
				console.log("Error");
			}else if(vm.text == '' || vm.text == undefined) {
				vm.ErrorText = "Вы ничего не написали!!!";
				vm.showError = true;
			}else {
		        $http.post('/api/quotes', {
		          text: vm.text
		        })
		        .success(function(response){
					console.log(response);
			        vm.quotes.push(response);
			        vm.text = "";
			        countProgress();
		        })
			    .error(function(response, status, headers, config) {
			    	console.log(response);
			    });
			}
		}

        vm.deleteQuote = function(quote){
            $http.delete('/api/quotes/' + quote.id)
            .success(function(){
                console.log('good');
                vm.quotes.splice(index, 1);
                countProgress();
            });
        }

        vm.editeQuote = function(quote){
            $http.put('/api/quotes/', {
            	index: index,
            	quote: quote
            })
            .success(function(){
            	console.log("good", quote);
            });
        }

	};