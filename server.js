var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var shortid = require('shortid')
var fs = require('fs');

var app = express();

app.set('port', process.env.PORT || 3000);

app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 1 }));


var obj;

//Апишка для вывода всех цитат.
app.get('/api/quotes', function(req, res, next){
	fs.readFile('./data.json', 'utf8', function (err, data) {
		if(err) return next(err);
		if(data) {
			obj = JSON.parse(data);
			res.json(obj.quotes);
		}
	});
});

//Апишка для создание цитаты
app.post('/api/quotes', function(req, res, next){
	fs.readFile('data.json', 'utf8', function readFileCallback(err, data){
	    if(err) return next(err);
	    else {
		    if(data) obj = JSON.parse(data);
		    obj.quotes.push({id: shortid.generate(), text:req.body.text});
		    json = JSON.stringify(obj);
		    fs.writeFile('./data.json', json, 'utf8', function (err) {
				if(err) return next(err);
				var quote = obj.quotes[obj.quotes.length - 1];
				res.json(quote);
			}); 
		}
	});
});

//Апишка для удаления цитаты
app.delete('/api/quotes/:id', function(req, res, next){
	fs.readFile('data.json', 'utf8', function readFileCallback(err, data){
	    if(err) return next(err);
	    else {
		    if(data) obj = JSON.parse(data);
            var index = obj.quotes.indexOf(req.params.id);
		    obj.quotes.splice(index, 1);
		    json = JSON.stringify(obj);
		    fs.writeFile('./data.json', json, 'utf8', function (err) {
	    		if(err) return next(err);
    			res.status(200).end();
			}); 
		}
	});
});

//Апишка для изменения цитаты
app.put('/api/quotes', function(req, res, next){
    console.log(req.body);
	fs.readFile('data.json', 'utf8', function readFileCallback(err, data){
	    if(err) return next(err);
	    else {
		    if(data) obj = JSON.parse(data);
		    obj.quotes.splice(req.body.index, 1, req.body.quote);
		    json = JSON.stringify(obj);
		    fs.writeFile('./data.json', json, 'utf8', function (err) {
	    		if(err) return next(err);
    			res.status(200).end();
			}); 
		}
	});
});


app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send({ message: err.message });
});

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});